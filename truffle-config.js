require('babel-register')

module.exports = {
  networks: {
    development: {
      host: "13.115.132.120",
      port: 8545,
      network_id: '3',
      gas: 4612188,
      gasPrice: 30000000000,
      // from:'0x98d6b4d574b1cba14c884e057a2932df4299415b'
    },
    testrpc: {
      host: "localhost",
      port: 8545,
      network_id: '3'
    }
  }
}
